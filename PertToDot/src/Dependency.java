/**
 * @(#) Dependency.java
 */
public class Dependency
{
    private Task startTask;
    
    private Task endTask;

    public Dependency(Task startTask, Task endTask) {
        this.startTask = startTask;
        this.endTask = endTask;
    }

    
    public Task getStartTask( )
    {
        return startTask;
    }
    
    public Task getEndTask( )
    {
        return endTask;
    }
    public boolean isCriticalPath1() {
        return (endTask.getLateFinish()-endTask.getDuration()==startTask.getLateFinish());
    }

    public String toDot() {
        StringBuilder sb = new StringBuilder();
        sb.append("\"").append(startTask.getID()).append("\"");
        sb.append("->");
        sb.append("\"").append(endTask.getID()).append("\"");
        if(isCriticalPath1() && startTask.isCriticalPath() && endTask.isCriticalPath()) {
            sb.append("[").append("style=\"bold\", color=\"red\"").append("]");
        }
        return sb.toString();
    }
}

