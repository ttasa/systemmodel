import java.io.*;
import java.util.*;

/**
 * @(#) PertChart.java
 */
public class PertChart
{
    private java.util.List<Task> tasks;

    public PertChart() {
        tasks = new ArrayList<>();
    }

    //parse input file, check for cycles and multiple start or end nodes
    public void parsePert(File input )
    {
        try {
            BufferedReader in = new BufferedReader(new FileReader(input));
            String line;
            String[] tokens;
            HashMap<String,Task> taskMap = new HashMap<>();
            List<String[]> dependencies = new ArrayList<>();
            while ((line = in.readLine()) != null) {
                tokens = line.split(",");
                if(tokens.length < 2) {
                    throw new RuntimeException("Invalid line in input file");
                }
                Task task = new Task(tokens[0], Integer.parseInt(tokens[1]));
                if(taskMap.containsKey(tokens[0])){
                	throw new RuntimeException("Task is not unique");
                }
                taskMap.put(tokens[0],task);
                dependencies.add(tokens);
            }
            in.close();
            for(String[] dep : dependencies) {
                Task endTask = taskMap.get(dep[0]);
                HashSet<Task> duplic = new HashSet<Task>();
                for(int i = 2; i < dep.length; i++) {
                    Task startTask = taskMap.get(dep[i]);
                    if (duplic.contains(startTask)){
                    	throw new RuntimeException("A preceding task is referenced multiple times");
                    }
                    duplic.add(startTask);
                    if(startTask == null) {
                        throw new RuntimeException("Dependency to undefined task");
                    }
                    
                    Dependency dependency = new Dependency(startTask, endTask);
                    startTask.getSuccessors().add(dependency);
                    endTask.getPredecessors().add(dependency);
                }
            }
            sortTasks(taskMap.values());
            checkSingleStartAndEnd();
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Input file not found.");
        } catch (IOException | NumberFormatException e)  {
            throw new RuntimeException("Error reading input file.");
        }
    }


    //calculate early and late finishes for all tasks to enable calculation of critical path
    public void calculatePath( )
    {
        for(Task task : tasks) {
            int maxPrevFinish = 0;
            for(Dependency pred : task.getPredecessors()) {
                Task predecessor = pred.getStartTask();
                if (predecessor.getEarlyFinish() > maxPrevFinish) {
                    maxPrevFinish = predecessor.getEarlyFinish();
                }
            }
            task.setEarlyFinish(maxPrevFinish + task.getDuration());
        }

        int projectFinishTime = tasks.get(tasks.size() - 1).getEarlyFinish();
        ListIterator<Task> li = tasks.listIterator(tasks.size());

        while(li.hasPrevious()) {
            Task task = li.previous();
            if(task.getSuccessors().size() == 0) {
                task.setLateFinish(task.getEarlyFinish());
            } else {
                int minNextStart = projectFinishTime;
                for(Dependency suc : task.getSuccessors()) {
                    Task successor = suc.getEndTask();
                    int sucLateStart = successor.getLateFinish() - successor.getDuration();
                    if (sucLateStart < minNextStart) {
                        minNextStart = sucLateStart;
                    }
                }
                task.setLateFinish(minNextStart);
            }
        }
        
    }

    //output this chart in dot format
    public void toDot(File output )
    {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(output));
            writer.write("digraph G {");
            writer.newLine();
            for(Task task : tasks) {
                writer.write(task.toDot());
                writer.newLine();
                for(Dependency dep : task.getSuccessors()) {
                    writer.write(dep.toDot());
                    writer.newLine();
                }
            }
            writer.write("}");
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException("Unable to write output file.");
        }

    }

    //Check that we have a single start and end node (we have at least one of both, otherwise there would be a cycle)
    private void checkSingleStartAndEnd() {
        boolean startFound=false, endFound = false;
        for(Task task : tasks) {
            if(task.getPredecessors().size() == 0) {
                if(startFound) {
                    throw new RuntimeException("Duplicate start nodes.");
                }
                startFound = true;
            }
            if(task.getSuccessors().size() == 0) {
                if(endFound) {
                    throw new RuntimeException("Duplicate end nodes.");
                }
                endFound = true;
            }
        }
    }

    //Algorithm taken from wikipedia
    private void sortTasks(Collection<Task> tasks) {
        HashMap<Task, Boolean> visited = new HashMap<>();
        for(Task task : tasks) {
            visit(task, visited);
        }
    }

    private void visit(Task task, HashMap<Task, Boolean> visited) {
        Boolean permanentlyMarked = visited.get(task);
        if(permanentlyMarked == null) {
            visited.put(task,false);
            for(Dependency dep : task.getPredecessors()) {
                visit(dep.getStartTask(),visited);
            }
            visited.put(task,true);
            tasks.add(task);
        } else if(!permanentlyMarked) {
            throw new RuntimeException("Input graph has cycles.");
        }


    }


}
