
import java.io.File;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("No input filename specified.");
        } else {
            try {
                String input = args[0];
                File inputFile = new File(input);
                String output = (inputFile.getName().contains(".") ? inputFile.getName().substring(0, inputFile.getName().lastIndexOf('.')) : inputFile.getName()) + ".dot";
                File outputFile = new File(output);
                PertChart pertChart = new PertChart();
                pertChart.parsePert(inputFile);
                pertChart.calculatePath();
                pertChart.toDot(outputFile);
            } catch (RuntimeException e) {
                System.err.println("Error: " + e.getMessage());
            }

        }
    }
}