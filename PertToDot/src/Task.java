import java.util.ArrayList;
import java.util.List;

/**
 * @(#) Task.java
 */
public class Task
{


    private java.util.List<Dependency> predecessors;
    
    private String ID;
    
    private int duration;

    private int earlyFinish;

    private int lateFinish;
    
    private java.util.List<Dependency> successors;

    public Task(String ID, int duration) {
        this.ID = ID;
        this.duration = duration;
        predecessors = new ArrayList<>();
        successors = new ArrayList<>();
    }

    public List<Dependency> getPredecessors() {
        return predecessors;
    }

    public String getID() {
        return ID;
    }

    public int getDuration() {
        return duration;
    }

    public List<Dependency> getSuccessors() {
        return successors;
    }

    public boolean isCriticalPath() {
        return ((lateFinish - earlyFinish) == 0);
    }

    public int getEarlyFinish() {
        return earlyFinish;
    }

    public void setEarlyFinish(int earlyFinish) {
        this.earlyFinish = earlyFinish;
    }

    public int getLateFinish() {
        return lateFinish;
    }

    public void setLateFinish(int lateFinish) {
        this.lateFinish = lateFinish;
    }
    

    public String toDot() {
        StringBuilder sb = new StringBuilder();
        sb.append("\"").append(ID).append("\"");
        sb.append("[shape=polygon, sides=4, ");
        if(isCriticalPath()) {
            sb.append("style=\"bold\", color=\"red\",");
        }
        sb.append("label=\"").append(ID).append("(duration: ").append(duration).append(")\"]");
        return sb.toString();
    }
}
